﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MarketPlayer.Survey.DAL;

namespace MarketPlayer.Survey.Models
{
    public enum QuestionType
    {
        Choice = 1,
        Text,
        ConceptWords
    }
}