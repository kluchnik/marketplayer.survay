﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketPlayer.Survey.Models
{
    public class ReportItem
    {
        public string QuestionCode { get; set; }
        public string QuestionText { get; set; }
        public int Count { get; set; }

        public QuestionType QuestionType { get; set; }
        public List<QuestionChoiceViewModel> QuestionChoices { get; set; } = new List<QuestionChoiceViewModel>();
    }

    public class QuestionChoiceViewModel
    {
        public string Text { get; set; }
        public int Value { get; set; }
        
    }
}