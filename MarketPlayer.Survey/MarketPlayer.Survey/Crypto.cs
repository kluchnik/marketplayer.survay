﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Rijndael256;

namespace MarketPlayer.Survey
{
    public class Crypto
    {
        private const string password = "Market@Music!Secret#Key";  // The password to encrypt the data with

        public static string Encrypt(string plaintext)
        {
            // Encrypt the string
            string ciphertext = Rijndael.Encrypt(plaintext, password, KeySize.Aes256);
            return ciphertext;
        }

        public static string Decrypt(string ciphertext)
        {
            // Decrypt the string
            var plaintext = Rijndael.Decrypt(ciphertext, password, KeySize.Aes256);
            return plaintext;
        }
    }
}