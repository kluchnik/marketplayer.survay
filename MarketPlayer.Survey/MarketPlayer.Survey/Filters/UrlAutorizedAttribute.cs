﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Mvc;

namespace MarketPlayer.Survey.Filters
{
    public class UrlAutorizedAttribute: System.Web.Mvc.AuthorizeAttribute
    {
        private const string SecretCode = "5ce66de6-0153-4331-b76b-84518bfe2d02";

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var mm_authorized = httpContext.Request.Cookies.AllKeys.Contains("mm_authorized");
            if (mm_authorized)
            {
                return true;
            }
            else if (httpContext.Request.QueryString.AllKeys.Contains("code"))
            {
                bool result = httpContext.Request.QueryString.Get("code") == SecretCode;
                if (result)
                {
                    httpContext.Response.Cookies.Add(new HttpCookie("mm_authorized", "approve"));
                }
                return result;
            }
            return base.AuthorizeCore(httpContext);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.NotFound);
          //  base.HandleUnauthorizedRequest(filterContext);
        }
    }
}