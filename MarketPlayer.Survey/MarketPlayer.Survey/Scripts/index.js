function init() {
    function getConceptHtml(){
        return `<div class="concept">
            <div class="concept-header"> 
                <span>Самые современные магазины, отели, заправки, рестораны уже тратят деньги на музыкальное оформление и видео экраны, но не всегда понимают связь этих инвестиций с прибылью компании.</span>
            </div>
            <div class="conpect-body">
                <div class="row">
                    <div style="width: 550px;float: left;text-align: justify;">                        
                        <div>
                            <p>Market Player - система, которая использует искусственный интеллект и данные о посетителях в розничной точке для таргетированной доставки медиа контента.<p>
                            <p>Подтвержденные на практике:</p>
                            <ul>
                                <li><span>Рост количества покупок и среднего чека достигается созданием позитивного опыта посещения с использованием качественного контента</span></li>
                                <li><span>Повышение эффективности рекламы и промоакций обеспечивается модулем клиентской аналитики. Market Player собирает данные о посетителях в режиме реального времени</span></li>
                                <li><span>Существенное сокращение расходов за счёт оптимизации затрат на оборудование, техническую поддержку и лицензии</span></li>
                            </ul>
                        </div>
                    </div>
                    <div>
                        <div class="photo"><img src="images/photo.png" ></div>
                    </div>                    
                </div>
            </div>
            <div class="concept-footer">
                <div class="center">
                    <span>Создайте эффективную систему управления медиа контентом с Market Player!</span>
                    <div class="nothing-button">
                        <div class="radio">
                            <label class="">
                                <input type="radio" id="nothing_words_radio">
                                <span class="check glyphicon glyphicon-ok"></span>
                                <span >Ничего</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>`;
    }


  function get_q8_randomized() {
    var data = [
        {
            type: "radiogroup", name: "q8_1", title: questionsMap["q8_1"], //"Премиальная / дорогая система",
            isRequired: true, choices: questionsChoicesMap["q8_1"]//  ["1", "2", "3", "4", "5", "6", "7", { text: "Затрудняюсь ответить", value: 0 }]
        },
        {
            type: "radiogroup", name: "q8_2", title: questionsMap["q8_2"], //  "Инновационная система, использующая высокие технологии",
            visibleIf: "{q8_1}>=0", isRequired: true, choices: questionsChoicesMap["q8_2"]// ["1", "2", "3", "4", "5", "6", "7", { text: "Затрудняюсь ответить", value: 0 }]
        },
        {
            type: "radiogroup", name: "q8_3", title: questionsMap["q8_3"], //"Надёжная система, которой можно доверять",
            visibleIf: "{q8_2}>=0", isRequired: true, choices: questionsChoicesMap["q8_3"]//["1", "2", "3", "4", "5", "6", "7", { text: "Затрудняюсь ответить", value: 0 }]
        },
        {
            type: "radiogroup", name: "q8_4", title: questionsMap["q8_4"],//"Профессиональный маркетинговый инструмент",
            visibleIf: "{q8_3}>=0", isRequired: true, choices: questionsChoicesMap["q8_4"]// ["1", "2", "3", "4", "5", "6", "7", { text: "Затрудняюсь ответить", value: 0 }]
        },
        {
            type: "radiogroup", name: "q8_5", title: questionsMap["q8_5"],//"Проста и удобна в использовании",
            visibleIf: "{q8_4}>=0", isRequired: true, choices: questionsChoicesMap["q8_5"]// ["1", "2", "3", "4", "5", "6", "7", { text: "Затрудняюсь ответить", value: 0 }]
        },
        {
            type: "radiogroup", name: "q8_6", title: questionsMap["q8_6"],//"Очень сложная система, требуется время, чтобы в ней разобраться",
            visibleIf: "{q8_5}>=0", isRequired: true, choices: questionsChoicesMap["q8_6"]// ["1", "2", "3", "4", "5", "6", "7", { text: "Затрудняюсь ответить", value: 0 }]
        },
        {
            type: "radiogroup", name: "q8_7", title: questionsMap["q8_7"], //"Современная, актуальная система",
            visibleIf: "{q8_6}>=0", isRequired: true, choices: questionsChoicesMap["q8_7"]// ["1", "2", "3", "4", "5", "6", "7", { text: "Затрудняюсь ответить", value: 0 }]
        },
        {
            type: "radiogroup", name: "q8_8", title: questionsMap["q8_8"],//"Креативное решение от креативной компании",
            visibleIf: "{q8_7}>=0", isRequired: true, choices: questionsChoicesMap["q8_8"]// ["1", "2", "3", "4", "5", "6", "7", { text: "Затрудняюсь ответить", value: 0 }]
        },
        {
            type: "radiogroup", name: "q8_9", title: questionsMap["q8_9"],//"Подходит только крупным компаниям с большой сетью торговых точек",
            visibleIf: "{q8_8}>=0", isRequired: true, choices: questionsChoicesMap["q8_9"]// ["1", "2", "3", "4", "5", "6", "7", { text: "Затрудняюсь ответить", value: 0 }]
        },
        {
            type: "radiogroup", name: "q8_10", title: questionsMap["q8_10"], //"Подходит для компаний любого размера",
            visibleIf: "{q8_9}>=0", isRequired: true, choices: questionsChoicesMap["q8_10"]//["1", "2", "3", "4", "5", "6", "7", { text: "Затрудняюсь ответить", value: 0 }]
        },
        {
            type: "radiogroup", name: "q8_11", title: questionsMap["q8_11"], //"Умная, интеллектуальная система, работает с данными",
            visibleIf: "{q8_10}>=0", isRequired: true, choices: questionsChoicesMap["q8_11"]//  ["1", "2", "3", "4", "5", "6", "7", { text: "Затрудняюсь ответить", value: 0 }]
        },
        {
            type: "radiogroup", name: "q8_12", title: questionsMap["q8_12"], //"Комплексное решение нескольких важных задач маркетинга и IT",
            visibleIf: "{q8_11}>=0", isRequired: true, choices: questionsChoicesMap["q8_12"]// ["1", "2", "3", "4", "5", "6", "7", { text: "Затрудняюсь ответить", value: 0 }]
        },
        {
            type: "radiogroup", name: "q8_13", title: questionsMap["q8_13"], //"С этой системой мы будем лучше понимать своих клиентов",
            visibleIf: "{q8_12}>=0", isRequired: true, choices: questionsChoicesMap["q8_13"]//["1", "2", "3", "4", "5", "6", "7", { text: "Затрудняюсь ответить", value: 0 }]
        },
        {
            type: "radiogroup", name: "q8_14", title: questionsMap["q8_14"],//"С её помощью можно увеличить лояльность покупателей",
            visibleIf: "{q8_13}>=0", isRequired: true, choices: questionsChoicesMap["q8_14"]//["1", "2", "3", "4", "5", "6", "7", { text: "Затрудняюсь ответить", value: 0 }]
        },
        {
            type: "radiogroup", name: "q8_15", title: questionsMap["q8_15"],//"Создаёт позитивную, располагающую атмосферу в торговых точках",
            visibleIf: "{q8_14}>=0", isRequired: true, choices: questionsChoicesMap["q8_15"]//["1", "2", "3", "4", "5", "6", "7", { text: "Затрудняюсь ответить", value: 0 }]
        },
        {
            type: "radiogroup", name: "q8_16", title: questionsMap["q8_16"], //"Эта система позволит увеличить прибыль нашей компании",
            visibleIf: "{q8_15}>=0", isRequired: true, choices: questionsChoicesMap["q8_16"]//["1", "2", "3", "4", "5", "6", "7", { text: "Затрудняюсь ответить", value: 0 }]
        }
    ];

      var shuffle = _.shuffle(data);
      shuffle[0].visibleIf = undefined;
      for (var shuffleIndex = 1; shuffleIndex < shuffle.length; shuffleIndex++) {
        shuffle[shuffleIndex].visibleIf = `{${shuffle[shuffleIndex - 1].name}}>=0`;
      }

      return shuffle;
    }

    var q8_shuffle_questions = [
        {
            type: "html",
            name: "info",
            html:
                "<h5>Подумайте о системе аудио и видео маркетинга для торговых сетей, описание которой Вы только что прочитали. Пожалуйста, скажите, насколько каждое из высказываний подходит этому продукту. Вы можете использовать любой балл от 1 до 7, наилучшим образом описывающий Ваше мнение, где 1 это совершенно не подходит, а 7 это подходит полностью.</h5>"
        }
  ];

    var q8_randomized_data = get_q8_randomized();
    for (var item in q8_randomized_data) {
        q8_shuffle_questions.push(q8_randomized_data[item]);
    }
    
    var json = { 
        cookieName: "myuniquesurveyid",
        title: "  ", //заглушка
        showProgressBar: "top",
        showQuestionNumbers: false,        
        questionTitleTemplate: "{title}",        
        locale: "ru",
        pageNextText: "Продолжить",
        pagePrevText: "hide_me",
        completeText: "Завершить",
        completedHtml: "<h5>БОЛЬШОЕ СПАСИБО. ЭТО ВСЕ ВОПРОСЫ, КОТОРЫЕ МЫ ХОТЕЛИ ВАМ ЗАДАТЬ. БЛАГОДАРИМ ЗА ПОМОЩЬ В РАБОТЕ!</h5>",
    triggers: [
                { type: "complete", name: "a1", operator: "equal", value: 3 },                
                { type: "complete", name: "a4", operator: "equal", value: 1 },                
                { type: "visible", name: "q2", operator: "greater", value: 2, questions: ["q2_1"] }                
               ],    
    pages: [

        {questions: [
        {
                type: "radiogroup", name: "s1", title: questionsMap["s1"],              
                 isRequired: true, 
                  colCount: 2,
                  choices: questionsChoicesMap["s1"]
                  //[{ text: "Мужской", value: 1 }, { text: "Женский", value: 2 }] 
            }            
        ]
    },{        
        questions: [
          { type: "text", isRequired: true, name: "s2", title: questionsMap["s2"]/*"Сколько Вам полных лет"*/, validators: [{ type: "numeric", minValue: 10, maxValue: 99 }] }
        ]
    }, {
        questions: [
            { 
                type: "radiogroup", 
                name: "s3", 
                title: questionsMap["s3"], //"Какую должность Вы занимаете в компании?",              
                isRequired: true,                               
                choices: questionsChoicesMap["s3"]
                /*
                [
                     { text: "Владелец или совладелец компании", value: 1 }, 
                     { text: "Директор предприятия, компании", value: 2 }, 
                     { text: "Руководитель высшего звена (IT-директор, директор по маркетингу, финансовый директор, коммерческий директор и т.д.)", value: 3 }, 
                     { text: "Руководитель среднего звена", value: 4 },
                     { text: "Квалифицированный специалист, менеджер (работа предполагает наличие высшего образования)", value: 5 } 
                    ]    */
            }                        
        ]
    },{
        questions: [
          {
            type: "text", isRequired: true, name: "s4", title: questionsMap["s4"] /*"Как именно называется ваша должность?"*/, validators: [
              { type: "text", minLength: 5, text: "Дайте более подробный ответ, пожалуйста" },
              { type: "mytext", minLength: 20 }]
          }
        ]
    }, {
        questions: [
            { 
                type: "radiogroup", 
                name: "s5", 
                title: questionsMap["s5"], //"Чем занимается Ваша организация, какова ее основная сфера деятельности?",              
                isRequired: true, 
                colCount: 2,                   
                choices: questionsChoicesMap["s5"]
               /* [
                     { text:"Автозаправки", value: 1 },
                     { text:"Розничная торговля одеждой и обувью", value: 2 },
                     { text:"Кафе, бары, рестораны", value: 3 },
                     { text:"Автосалоны", value: 4 },
                     { text:"Парфюмерия", value: 5 },
                     { text:"Розничная торговля продуктами питания", value: 6 },
                     { text:"Гостиницы", value: 7 },
                     { text:"Банки", value: 8 },
                     { text:"Фитнес-центры", value: 9 },
                     { text:"Торговые центры", value: 10 },
                     { text:"Другое", value: 99 }
                    ]*/
            }                        
        ]
    },{ 
        questions: [
            { 
                type: "html", 
                name: "concept",                 
                html:"<h5>Сейчас мы покажем Вам описание новой системы аудио и видео маркетинга для торговых сетей. Пожалуйста, внимательно прочитайте его и ответьте на несколько вопросов.</h5>"                      
            }]
    },{        
        theme:"white",
        questions: [
            { 
                type: "html", 
                name: "concept",                 
                html: getConceptHtml() + "<style> .progress, .nothing-button { display: none }</style>"
            }]
    },{
          questions: [
            {
              type: "radiogroup", name: "q1", title: questionsMap["q1"],//"Пожалуйста, скажите, насколько в целом Вам нравится или не нравится описание этой системы?",
              isRequired: true, choices: questionsChoicesMap["q1"] //["1", "2", "3", "4", "5", "6", "7"] 
            }                        
        ]
    },{
          questions: [
            {
              type: "comment", name: "q1_1", title: questionsMap["q1_1"],//"Скажите, что Вам понравилось в описании этой системы? Пожалуйста, постарайтесь дать наиболее развернутый ответ. А что еще Вам понравилось?",              
              isRequired: true, validators: [{ type: "text", minLength: 5, text: "Дайте более подробный ответ, пожалуйста" }, { type: "mytext", minLength: 20 }]
            }                        
        ]
    },{
          questions: [
              {
              type: "comment", name: "q1_2", title: questionsMap["q1_2"]/* "А что Вам не понравилось в описании этой системы?"*/,
              isRequired: true, validators: [{ type: "text", minLength: 5, text: "Дайте более подробный ответ, пожалуйста" }, { type: "mytext", minLength: 20 }]
              }                        
        ]
    },{
          questions: [
            {
              orientation: 'vertical',
              type: "radiogroup", 
              name: "q2", 
              title: questionsMap["q2"], //"Основываясь на описании, какова вероятность того, что Ваша компания свяжется с представителями Market Player для того, чтобы узнать о системе подробнее и получить стоимость её внедрения?", 
              isRequired: true,
              choices: questionsChoicesMap["q2"]

            /*[
                     {text: "Определенно свяжется", value: 1},
                     {text: "Скорее свяжется", value: 2},
                     {text: "Может свяжется, а может нет", value: "3"},
                     {text: "Скорее не свяжется", value: 4},
                     {text: "Определенно не свяжется", value: 5},
                    ]     */
            }                        
        ]
    },{
          questions: [
            {
                 type: "comment", 
                 name: "q2_1", 
                 title: questionsMap["q2_1"], //"Вы сказали, что не свяжетесь с компанией-производителем с вопросами о внедрении этой системы. Скажите, пожалуйста, почему?", 
                 isRequired: true, validators: [{ type: "text", minLength: 5 }, { type: "mytext", minLength: 20 }]
            }                        
        ]
    },
    {questions: [         
         { 
                type: "html", 
                name: "info", 
                html:"<h5>Подумайте о системе аудио и видео маркетинга для торговых сетей, описание которой Вы только что прочитали. Пожалуйста, скажите, насколько каждое из высказываний подходит этому продукту. Вы можете использовать любой балл от 1 до 7, наилучшим образом описывающий Ваше мнение, где 1 это совершенно не подходит, а 7 это подходит полностью.</h5>"             
            },
         {
           type: "radiogroup", name: "q3", title: questionsMap["q3"],// "Эта система уникальна и отличается от других похожих инструментов",
           isRequired: true, choices: questionsChoicesMap["q3"]// ["1", "2", "3", "4", "5", "6", "7"] 
            } ,
            {
              type: "radiogroup", name: "q4", title: questionsMap["q4"] /*"Этот продукт подходит мне и моей компании"*/, visibleIf: "{q3}>=0",
              isRequired: true, choices: questionsChoicesMap["q4"] //["1", "2", "3", "4", "5", "6", "7"] 
            } ,
            {
              type: "radiogroup", name: "q5", title: questionsMap["q5"] /*"Я доверяю тому, о чем говорится в этом описании"*/, visibleIf: "{q4}>=0",
              isRequired: true, choices: questionsChoicesMap["q5"]  //["1", "2", "3", "4", "5", "6", "7"] 
            } ,
            {
              type: "radiogroup", name: "q6", title: questionsMap["q6"] /*"Мне понятно то, что говорится в этом описании"*/, visibleIf: "{q5}>=0",
              isRequired: true, choices: questionsChoicesMap["q6"] //["1", "2", "3", "4", "5", "6", "7"] 
            } 
            ]
        },{
          questions: [
            {
              orientation: 'vertical',
              type: "radiogroup", 
              name: "q7", 
              title: questionsMap["q7"], //"Как Вы думаете, насколько название Market Player подходит или не подходит системе, описание которой Вы только что прочитали?", 
              isRequired: true,
              choices: questionsChoicesMap["q7"]
            /*[
                     {text: "Полностью подходит", value: 1},
                     {text: "Скорее подходит", value: 2},
                     {text: "Отчасти подходит, отчасти нет", value: "3"},
                     {text: "Скорее не соответствует", value: 4},
                     {text: "Совершенно не подходит", value: 5},
                    ]     */
            }                        
        ]
    }, {
            name: "q8_randomize",
            questions: q8_shuffle_questions
        },{        
        questions: [
            { 
                type: "html", 
                name: "info q9-13",                 
                html:"<h5>В следующих вопросах мы попросим Вас выделить некоторые слова или фразы с помощью мышки. Если вы кликнете в уже выделенные слова ещё раз, то выделение отменяется.</h5>"                      
            }]
    },
        {        
            theme:"white",
            questions: [
            { 
              type: "text", 
              name: "q9", 
              renderAs: "conceptwords",
              title: questionsMap["q9"]//"Пожалуйста, выделите слова в этом описании, которые считаете наиболее важными для Вас, если такие есть. Пожалуйста, выделите все подходящие слова. Если таких слов нет, выберите «Ничего»."
              , validators: [{ type: "conceptwords" }]
            }]
    },{        
        theme:"white",
        questions: [
            { 
              type: "text", 
              name: "q10", 
              renderAs: "conceptwords",
              title: questionsMap["q10"] //"Далее выделите слова, которые Вы считаете наименее важными для Вас. Пожалуйста, выберите все подходящие слова. Если таких слов нет, выберите «Ничего»."
              , validators: [{ type: "conceptwords" }]
            }]
    },{        
        theme:"white",
        questions: [
            { 
              type: "text", 
              name: "q11", 
              renderAs: "conceptwords",
              title: questionsMap["q11"]//"Теперь выделите то, что Вы считаете особенно новым и отличающим этот продукт от других, если есть? Пожалуйста, выберите все подходящие слова. Если таких слов нет, пожалуйста, выберите «Ничего»."
              , validators: [{ type: "conceptwords" }]
            }]
    },{        
        theme:"white",
        questions: [
            { 
              type: "text", 
                name: "q12", 
                renderAs: "conceptwords",
                title: questionsMap["q12"]//"Теперь выберите слова, которые Вы считаете непонятными и сложными для понимания, если такие есть. Пожалуйста, выберите все подходящие слова. Если таких слов нет, выберите «Ничего»."
              , validators: [{ type: "conceptwords" }]
            }]
    },{        
        theme:"white",
        questions: [
            { 
            type: "text", 
                name: "q13", 
                renderAs: "conceptwords",
                title: questionsMap["q13"]// "Наконец выберите слова, которые, на Ваш взгляд, не вызывают доверия, если такие есть. Пожалуйста, выберите все подходящие слова. Если таких слов нет, выберите «Ничего»."
                , validators: [{ type: "conceptwords" }] 
            }]
    },{        
        questions: [
            { 
                type: "html", 
                name: "info a",                 
                html:"<h5>Сейчас несколько вопросов о Вашей организации, нам необходима эта информация для возможности обобщения результатов опроса, и никаким другим образом она использоваться не будет.</h5>"
            }]
    },
    {
          questions: [
            { 
                type: "radiogroup", 
                name: "a1", 
                title: questionsMap["a1"], // "Какова будет степень Вашего участия в принятии решения, если ваша компания будет обсуждать внедрение системы Market Player, описание которой вы сейчас прочитали, или подобной?", 
                isRequired: true,
                choices: questionsChoicesMap["a1"]
                /*[
                        {text: "Вы будете лично принимать решение", value: 1},
                        {text: "Вы будете участвовать в принятии решения", value: 2},
                        {text: "Не будете участвовать в принятии решения", value: 3}                     
                    ]     */
            }
        ]
    },
    {
          questions: [
            { 
                type: "radiogroup", 
                name: "a2", 
                title: questionsMap["a2"], // "Сколько всего сотрудников работает в Вашей организации с учетом всех филиалов, если они есть?", 
                isRequired: true,
                choices: questionsChoicesMap["a2"]
                /*[
                        {text: "Менее 20 человек", value: 1},
                        {text: "20-100 человек", value: 2},
                        {text: "101-250 человек", value: 3},
                        {text: "251-500 человек", value: 4},
                        {text: "501-1000 человек", value: 5},
                        {text: "Более 1000 человек", value: 6},
                        {text: "Затрудняюсь ответить", value: 99},
                    ]     */
            }
        ]
    },
    {
          questions: [
            { 
                type: "radiogroup", 
                name: "a3", 
                title: questionsMap["a3"], //"Какое количество торговых точек у вашей компании? ", 
                isRequired: true,
                choices: questionsChoicesMap["a3"]
                /*[
                        { text: "Менее 20", value: 1 },
                        { text: "21-50", value: 2 },
                        { text: "51-100", value: 3 },
                        { text: "101-500", value: 4 },
                        { text: "501-2000", value: 5 },
                        { text: "Более 2000", value: 6 }
                    ]     */
            }
        ]
    },
    {
          questions: [
            { 
                type: "radiogroup", 
                name: "a4", 
                title: questionsMap["a4"], //"Торговые точки Вашей компании находятся в одном городе или в нескольких городах?", 
                isRequired: true,
                choices: questionsChoicesMap["a4"]
                /*[
                        { text: "В одном городе", value: 1 },
                        { text: "Нескольких городах", value: 2 }                        
                    ]     */
            }
        ]
    },
    {
          questions: [            
            { type: "text", isRequired: true, name: "a5", title: questionsMap["a5"]/* "Укажите среднее количество торговых точек в одном городе?"*/, validators: [{ type: "numeric" }] }
        ]
    }
    ]//pages
    };


    var json2 = {
        //cookieName: "myuniquesurveyid",
        title: "  ", //заглушка
        showProgressBar: "top",
        showQuestionNumbers: false,
        questionTitleTemplate: "{title}",
        locale: "ru",
        pageNextText: "Продолжить",
        pagePrevText: "hide_me",
        completeText: "Завершить",
        completedHtml: "<h5>БОЛЬШОЕ СПАСИБО. ЭТО ВСЕ ВОПРОСЫ, КОТОРЫЕ МЫ ХОТЕЛИ ВАМ ЗАДАТЬ. БЛАГОДАРИМ ЗА ПОМОЩЬ В РАБОТЕ!</h5>",
        triggers: [
            { type: "complete", name: "exit1", operator: "equal", value: "Yes" },
            { type: "complete", name: "exit2", operator: "equal", value: "Yes" },
            { type: "visible", name: "q2", operator: "greater", value: 2, questions: ["q2_1"] }
        ],
        pages: [

            {
                questions: [
                    {
                        type: "radiogroup", name: "s1", title: "Отметьте свой пол, пожалуйста",
                        isRequired: true,
                        colCount: 2,
                        choices: [{ text: "Мужской", value: 1 }, { text: "Женский", value: 2 }]
                    }
                ]
            }, {
                theme: "white",
                questions: [
                  {
                        type: "text",
                        name: "q9",
                        renderAs: "conceptwords",
                        title: "Пожалуйста, выделите слова в этом описании, которые считаете наиболее важными для Вас, если такие есть. Пожалуйста, выделите все подходящие слова. Если таких слов нет, выберите «Ничего»."
                    }]
            }, {
                theme: "white",
                questions: [
                    {
                        type: "text",
                        name: "q10",
                        renderAs: "conceptwords",
                        title: "Далее выделите слова, которые Вы считаете наименее важными для Вас. Пожалуйста, выберите все подходящие слова. Если таких слов нет, выберите «Ничего»."
                    }]
            }
        ]//pages
    };

var myCss = {        
        progress: "progress"
   };
   
var widget = {
    name: "mmradio",
    isDefaultRender: true,
    isFit : function(question) { return question.getType() === 'radiogroup'; },
    afterRender: function(question, el) {
        //console.log(el);
        $(el).find('.form-inline .radio label').each(function(idx, x) {
            $(x).append($('<span class="check glyphicon glyphicon-ok"></span>'));
      });               

        $(el).find('.form-vertical .radio label').each(function (idx, x) {
            $(x).append($('<span class="check glyphicon glyphicon-ok"></span>'));
        });  

        if (question.orientation === 'vertical') {
            $(el).find('.form-inline').eq(0).removeClass('form-inline').addClass('form-vertical');
        } else {
          $(el).find('.form-vertical').eq(0).removeClass('form-vertical').addClass('form-inline');
        }
    }
}
Survey.CustomWidgetCollection.Instance.addCustomWidget(widget);

Survey.JsonObject.metaData.addProperty("text", {name: "renderAs", default: "standard", choices: ["standard", "conceptwords"]});
Survey.JsonObject.metaData.addProperty("page", { name: "theme", default: "blue", choices: ["blue", "white"] });

  Survey.JsonObject.metaData.addProperty("question", { name: "orientation", default: "horizontal", choices: ["horizontal", "vertical"] });

var widgetSelectConpectWords = {
    name: "conceptwords",
    isFit : function(question) { return question["renderAs"] === 'conceptwords'; },
    isDefaultRender: true,
    afterRender: function (question, el) {
            $(el).html(getConceptHtml());
        
            if(!question.value){
              question.value = '[conceptwords];';
            }
    
            $(el).find("p").each(function(idx, item){
                var words = $(this).text().split( /\s+/ );            
                var text = words.join( "</word> <word>" );
                $(this).html( "<word>" + text + "</word>" );
            });

            $(el).find( "span" ).each(function(idx, item){
                var words = $(this).text().split( /\s+/ );            
                var text = words.join( "</word> <word>" );
                $(this).html( "<word>" + text + "</word>" );
            });

            $(el).find("word").on( "click", function() {
                var word = $(this).text();

                if (word === 'Ничего') {
                    $(el).find('.highlight').removeClass("highlight");
                    question.value = '[conceptwords];Ничего;';
                } else {
                    $('#nothing_words_radio').prop('checked', false);
                    question.value.replace('Ничего;');
                    if ($(this).hasClass('highlight')) {
                        $(this).removeClass('highlight');

                        if (question.value) {
                            question.value.replace(word, '');
                        } //_.without(question.value, word);                     
                    } else {
                        $(this).addClass('highlight');
                        if (question.value.indexOf(word) === -1) {
                            question.value += word + ';';
                        }
                    }
                }
            });

            /*
            $(el).mouseup(function(e) {
                //alert( "Handler for .mouseup() called." );
                 var selection = window.getSelection() || document.getSelection() || document.selection.createRange();
                    var word = $.trim(selection.toString());
                    if(word != '') {
                        alert(word);
                    }
                    selection.collapse();
                    e.stopPropagation();
            });*/
    }
}

Survey.CustomWidgetCollection.Instance.addCustomWidget(widgetSelectConpectWords);


    var MyTextValidator = (function (_super) {
        Survey.__extends(MyTextValidator, _super);
        function MyTextValidator() {
            _super.call(this);
        }
        MyTextValidator.prototype.getType = function () { return "mytextvalidator"; };
        MyTextValidator.prototype.validate = function (value, name) {
            if (!value || value.trim().length === 0) {
                return new Survey.ValidatorResult(null, new Survey.CustomError(this.getErrorText(name)));
            }
            return null;
        };
        
        MyTextValidator.prototype.getDefaultErrorText = function (name) {
          return "Ответ должен содержать текст";
        }
        return MyTextValidator;
    })(Survey.SurveyValidator);
    Survey.MyTextValidator = MyTextValidator;
    Survey.JsonObject.metaData.addClass("mytextvalidator", [], function () { return new MyTextValidator(); }, "surveyvalidator");



    var ConceptWordstValidator = (function (_super) {
        Survey.__extends(MyTextValidator, _super);
        function MyTextValidator() {
            _super.call(this);
        }
        MyTextValidator.prototype.getType = function () { return "conceptwordsvalidator"; };
        MyTextValidator.prototype.validate = function (value, name) {
          if (!value || value === '[conceptwords];') {
                return new Survey.ValidatorResult(null, new Survey.CustomError(this.getErrorText(name)));
            }
            return null;
        };

        MyTextValidator.prototype.getDefaultErrorText = function (name) {
          return "Пожалуйста, выделите все подходящие слова. Если таких слов нет, выберите «Ничего».";
        }
        return MyTextValidator;
    })(Survey.SurveyValidator);
    Survey.MyTextValidator = MyTextValidator;
    Survey.JsonObject.metaData.addClass("conceptwordsvalidator", [], function () { return new ConceptWordstValidator(); }, "surveyvalidator");


    Survey.defaultBootstrapCss.navigationButton = "btn btn-primary";
    Survey.Survey.cssType = "bootstrap";
    var model = new Survey.Model(json);
    window.survey = model;

    function fixingCustomElements(sender){           
        var progress = sender.getProgress();
        $('.panel-heading span').text('Пройдено ' + progress + '%');
        $('span:contains(Затрудняюсь ответить)').parent().addClass('gray-color-answer');

    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }


    var enableAutoPageScroll = false;
    model.currentPageNo = getParameterByName('skip', location.href) || 0;


    //Create showdown mardown converter
    var converter = new showdown.Converter();
    model.onTextMarkdown.add(function (survey, options) {
        //convert the mardown text to html
        var str = converter.makeHtml(options.text);
        //remove root paragraphs <p></p>
        str = str.substring(3);
        str = str.substring(0, str.length - 4);
        //set html
        options.html = str;
    });
    $("#surveyElement").Survey({
      
            model:model,
            css: myCss,
            onComplete: function(sender, opts) {
                console.log('onComplete'); //sender.data
                console.log(sender.data);

                $.ajax({
                    type: "POST",
                    url: '/api/complete',
                    data: JSON.stringify(sender.data),
                    success: function(result) {
                        //alert('ok');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Произошла ошибка при отправке данных');
                    },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json"
                });
            },
            onAfterRenderQuestion: function(sender, opts){                                
                if(enableAutoPageScroll){
                    $("html, body").animate({ scrollTop: $(document).height() }, 1000);                    
                }
                enableAutoPageScroll = true;
                 
                fixingCustomElements(sender);
            },
            onCurrentPageChanged: function(sender, opts){  
               //console.log('onCurrentPageChanged')
               //
               enableAutoPageScroll = false;
                if(opts.newCurrentPage.theme && opts.newCurrentPage.theme === 'white'){
                    $('body').attr('class', 'white-theme');
                } else {
                    $('body').attr('class', 'blue-theme');
                }

                if (history.pushState) {
                    var page = opts.newCurrentPage.visibleIndex;
                    var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?p=' + page;
                    window.history.pushState({ path: newurl }, '', newurl);
                }
                
            },
            onVisibleChanged: function(){
             //   console.log('onVisibleChanged');
            }

    });

}


if(!window["%hammerhead%"]) {
    init();    
    $('.panel-footer').width($('.panel-body').width());
    $( window ).resize(function() {
        $('.panel-footer').width($('.panel-body').width());
    });
}