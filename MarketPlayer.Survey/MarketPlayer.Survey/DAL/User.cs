﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketPlayer.Survey.DAL
{
    public class User
    {

        public int Id { get; set; }
        public Guid UserKey { get; set; }
        public string Email { get; set; }

        public DateTime? RegisteredDateUTC { get; set; } = DateTime.UtcNow;
    }
}