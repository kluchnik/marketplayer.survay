﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketPlayer.Survey.DAL
{
    public class AnswerValue
    {
        public int Id { get; set; }
        public string Value { get; set; }

        public int AnswerId { get; set; }
        public virtual Answer Answer { get; set; }
    }
}