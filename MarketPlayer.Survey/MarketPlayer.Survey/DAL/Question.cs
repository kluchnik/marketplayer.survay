﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using MarketPlayer.Survey.Models;


namespace MarketPlayer.Survey.DAL
{
    public class Question
    {
        public int Id { get; set; }

        [MaxLength(50)]
        [Column(TypeName = "NVARCHAR")]
        [Index(IsUnique = true)]
        public string Code { get; set; }
        public string Text { get; set; }
        public QuestionType QuestionType { get; set; }

        public List<QuestionChoice> Choices { get; set; }
    }
}