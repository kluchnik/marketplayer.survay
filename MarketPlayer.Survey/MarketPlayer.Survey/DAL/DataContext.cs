﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MarketPlayer.Survey.DAL
{
    public class DataContext: DbContext
    {
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<QuestionChoice> QuestionChoices { get; set; }
        public  DbSet<AnswerValue> AnswerValues { get; set; }
        public DbSet<User> Users { get; set; }

        public DataContext() : base("MarketPlayerDbContext")
        {
        }

        static DataContext()
        {
            System.Data.Entity.Database.SetInitializer(new SurveyDbInitializer());
        }


        public static DataContext CreateReadOnly()
        {
            var context = new DataContext();
            var configuration = context.Configuration;
            configuration.AutoDetectChangesEnabled = false;
            configuration.EnsureTransactionsForFunctionsAndCommands = false;
            //configuration.LazyLoadingEnabled = false;
            configuration.ProxyCreationEnabled = false;
            configuration.UseDatabaseNullSemantics = false;
            return context;
        }
    }
}