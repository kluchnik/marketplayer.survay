﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketPlayer.Survey.DAL
{
    public class QuestionChoice
    {
        public int Id { get; set; }

        public string Text { get; set; }
        public int Value { get; set; }

        public int QuestionId { get; set; }
        public virtual Question Question { get; set; }
    }
}