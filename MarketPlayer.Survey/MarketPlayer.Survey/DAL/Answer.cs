﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MarketPlayer.Survey.DAL
{
    public class Answer
    {
        public int Id { get; set; }
        
        public virtual User User { get; set; }
        public int UserId { get; set; }

        public int QuestionId { get; set; }
        public virtual Question Question { get; set; }
        public List<AnswerValue> Values { get; set; }

        public DateTime? PublishedDateUTC { get; set; } = DateTime.UtcNow;

        [StringLength(20)]
        public string IpAddress { get; set; }
    }
}