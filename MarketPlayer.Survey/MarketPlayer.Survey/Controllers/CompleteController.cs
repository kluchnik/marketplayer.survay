﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using MarketPlayer.Survey.DAL;

namespace MarketPlayer.Survey.Controllers
{
    public class CompleteController : ApiController
    {
        public async Task Post([FromBody]Dictionary<string, string> values)
        {
            try
            {
                var cookie = Request.Headers.GetCookies("mm_unique_key").First();

                var tokenEmail = Request.Headers.GetCookies("mm_token").FirstOrDefault();
                string email = tokenEmail == null ? null : Encoding.UTF8.GetString(Convert.FromBase64String(tokenEmail["mm_token"].Value));

                Guid unique = Guid.Parse(cookie["mm_unique_key"].Value);
                using (var db = new DataContext())
                {
                    var questions = await db.Questions.ToListAsync();
                    var map = questions.ToDictionary(x => x.Code);
                    var user = await db.Users.FirstOrDefaultAsync(x => x.UserKey == unique && x.Email == email);
                    if (user == null)
                    {
                        user = new User
                        {
                            Email = email,
                            UserKey = unique
                        };

                        db.Users.Add(user);
                        await db.SaveChangesAsync();
                    }

                    foreach (var value in values)
                    {
                        if (map.ContainsKey(value.Key))
                        {
                            var answer = new Answer
                            {
                                Question = map[value.Key],
                                User = user,
                                Values = new List<AnswerValue>(),
                                IpAddress = HttpContext.Current != null ? HttpContext.Current.Request.UserHostAddress : ""
                            };

                            if (value.Value.StartsWith("[conceptwords]"))
                            {
                                var words = value.Value.Replace("[conceptwords]", string.Empty)
                                    .Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries);
                                foreach (var word in words)
                                {
                                    answer.Values.Add(new AnswerValue
                                    {
                                        Value = word
                                    });
                                }
                            }
                            else
                            {
                                answer.Values.Add(new AnswerValue
                                {
                                    Value = value.Value
                                });
                            }

                            db.Answers.Add(answer);
                        }
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var errorFile = HostingEnvironment.MapPath("~/App_Data/errors.log");
                System.IO.File.AppendAllText(errorFile, ex.Message);
                System.IO.File.AppendAllText(errorFile, ex.StackTrace);
                System.IO.File.AppendAllText(errorFile, "Values:" + values.Aggregate("", (x, y) => y + ";" + x ));
            }
        }
    }
}
