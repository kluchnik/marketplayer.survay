﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MarketPlayer.Survey.DAL;
using Newtonsoft.Json;

namespace MarketPlayer.Survey.Controllers
{
    public class HomeController : Controller
    {
        
        public  async Task<ActionResult> Index(string token)
        {
            var cookie = Request.Cookies["mm_unique_key"];
            if (cookie == null)
            {
                cookie = new HttpCookie("mm_unique_key", Guid.NewGuid().ToString());
                cookie.Expires = DateTime.Now.AddYears(10);
                Response.Cookies.Add(cookie);
            }

            var tokenCookie = Request.Cookies["mm_token"];
            if (tokenCookie == null && !string.IsNullOrEmpty(token))
            {
                tokenCookie = new HttpCookie("mm_token", HttpUtility.UrlDecode(token));
                tokenCookie.Expires = DateTime.Now.AddYears(10);
                Response.Cookies.Add(tokenCookie);
            }

            using (var db = new DataContext())
            {
                var questions = await db.Questions.AsNoTracking().ToListAsync();
                var choices = await db.QuestionChoices.AsNoTracking().ToListAsync();
                ViewBag.Questions = JsonConvert.SerializeObject(questions.ToDictionary(x => x.Code, x => x.Text));

                var group = choices.GroupBy(x => questions.First(q => q.Id == x.QuestionId).Code);
                
                ViewBag.QuestionChoices = JsonConvert.SerializeObject(group.ToDictionary(x => x.Key, x => x.ToList().Select(y => new { text = y.Text, value = y.Value })));
            }

            return View();
        }
    }
}
