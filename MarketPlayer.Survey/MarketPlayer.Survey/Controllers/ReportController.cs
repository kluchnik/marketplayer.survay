﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MarketPlayer.Survey.DAL;
using System.Threading.Tasks;
using ClosedXML.Excel;
using MarketPlayer.Survey.Filters;
using MarketPlayer.Survey.Models;
using SpreadsheetLight;

namespace MarketPlayer.Survey.Controllers
{
    [UrlAutorized]
    public class ReportController : Controller
    {
        // GET: Report
        public async Task<ActionResult> Index(string code)
        {
            var list = new List<ReportItem>();

            using (var db = DataContext.CreateReadOnly())
            {
                var questions = await db.Questions.Include(x => x.Choices).AsNoTracking().ToListAsync();
                var answers = await db.Answers.Include(x => x.Values).AsNoTracking().ToListAsync();

              //  var q = questions.First(x => x.QuestionType == QuestionType.Choice);
                foreach (var q in questions.Where(x => x.QuestionType == QuestionType.Choice))
                {
                    var ans = answers.Where(x => x.QuestionId == q.Id).ToList();
                    var item = new ReportItem()
                    {
                        QuestionType = QuestionType.Choice,
                        QuestionCode = q.Code,
                        QuestionText = q.Text
                    };

                    foreach (var choice in q.Choices)
                    {
                        item.QuestionChoices.Add(new QuestionChoiceViewModel()
                        {
                            Text = choice.Text,
                            Value = ans.Count(x => x.Values.Any(v => v.Value == choice.Value.ToString()))
                        });
                    }
                    list.Add(item);
                }
              
            }
            return View(list);
        }

        public async Task<ActionResult> Build(string code)
        {
            using (SLDocument sl = new SLDocument())
                {
                    using (var db = DataContext.CreateReadOnly())
                    {
                        var query = (
                            from a in db.Answers
                            join q in db.Questions on a.QuestionId equals q.Id
                            join av in db.AnswerValues on a.Id equals av.AnswerId into ans from answerLeft in ans.DefaultIfEmpty()
                            select new
                            {
                                UserKey = a.User.UserKey,
                                QuesionCode = q.Code,
                                QuestionText = q.Text,
                                QuetionType = q.QuestionType,
                                Value = answerLeft == null ? null : answerLeft.Value
                            }
                        );

                        var test = await query.ToListAsync();
                        var questions = await db.Questions.Include(x => x.Choices).AsNoTracking().ToListAsync();
                        

                        var cellIndexes = new Dictionary<string, int>();
                        for (var index = 0; index < questions.Count; index++)
                        {
                            var question = questions[index];
                            cellIndexes.Add(question.Code, index + 1);
                            sl.SetCellValue(CreateColumnName(index + 1, 1), question.Code);
                        }

                        uint row = 2;
                        foreach (var answer in test.GroupBy(x => x.UserKey))
                        {
                            foreach (var answer1 in answer.ToList().GroupBy(x => x.QuesionCode))
                            {
                                foreach (var qqq in answer1)
                                {
                                    var answersList = answer1.ToList();
                                    if (answersList.Count == 1)
                                    {
                                        int number;
                                        if (int.TryParse(answersList[0].Value, out number))
                                        {
                                            sl.SetCellValue(CreateColumnName(cellIndexes[qqq.QuesionCode], row), number);
                                        }
                                        else
                                        {
                                            sl.SetCellValue(CreateColumnName(cellIndexes[qqq.QuesionCode], row), answersList[0].Value);
                                        }
                                    }
                                    else if (answersList.Count > 1)
                                    {
                                        sl.SetCellValue(CreateColumnName(cellIndexes[qqq.QuesionCode], row), answersList.Aggregate("", (x,y) => y.Value + ";" + x));
                                    }
                                    
                                }
                                
                            }

                            row++;
                        }
                    }

                sl.SaveAs(Server.MapPath("~/App_Data/report.xlsx"));
                Response.AppendHeader("content-disposition", "attachment; filename=report.xlsx");
                return File(Server.MapPath("~/App_Data/report.xlsx"), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            }
        }


        private static string CreateColumnName(int columnIndex, uint rowIndex)
        {
            //  Convert a zero-based column index into an Excel column reference  (A, B, C.. Y, Y, AA, AB, AC... AY, AZ, B1, B2..)
            //
            //  eg  GetExcelColumnName(0) should return "A"
            //      GetExcelColumnName(1) should return "B"
            //      GetExcelColumnName(25) should return "Z"
            //      GetExcelColumnName(26) should return "AA"
            //      GetExcelColumnName(27) should return "AB"
            //      ..etc..
            //
            if (columnIndex < 26)
                return ((char)('A' + columnIndex)).ToString() + rowIndex;

            char firstChar = (char)('A' + (columnIndex / 26) - 1);
            char secondChar = (char)('A' + (columnIndex % 26));

            return string.Format("{0}{1}{2}", firstChar, secondChar, rowIndex);
        }
    }
}