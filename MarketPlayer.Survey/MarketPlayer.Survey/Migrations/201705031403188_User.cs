namespace MarketPlayer.Survey.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class User : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserKey = c.Guid(nullable: false),
                        Email = c.String(),
                        RegisteredDateUTC = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Answers", "UserId", c => c.Int(nullable: false));
            CreateIndex("dbo.Answers", "UserId");
            AddForeignKey("dbo.Answers", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            
            DropColumn("dbo.Answers", "UserKey");
            DropColumn("dbo.Answers", "Email");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Answers", "Email", c => c.String());
            AddColumn("dbo.Answers", "UserKey", c => c.Guid(nullable: false));
            DropForeignKey("dbo.Answers", "UserId", "dbo.Users");
            DropIndex("dbo.Answers", new[] { "UserId" });
            DropColumn("dbo.Answers", "UserId");
            DropTable("dbo.Users");
        }
    }
}
