using System.Collections.Generic;
using MarketPlayer.Survey.DAL;
using MarketPlayer.Survey.Models;

namespace MarketPlayer.Survey.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<MarketPlayer.Survey.DAL.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DataContext context)
        {
            base.Seed(context);

            if (context.Questions.Any())
            {
                return;
            }

            context.Questions.Add(new Question()
            {
                Code = "s1",
                Text = "�������� ���� ���, ����������",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){Text = "�������" , Value = 1 },
                    new QuestionChoice() {Text = "�������", Value = 2 }
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "s2",
                QuestionType = QuestionType.Text,
                Text = "������� ��� ������ ���?"
            });
            context.Questions.Add(new Question()
            {
                Code = "s3",
                QuestionType = QuestionType.Choice,
                Text = "����� ��������� �� ��������� � ��������?",
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "�������� ��� ���������� ��������", Value = 1 },
                    new QuestionChoice(){ Text = "�������� �����������, ��������", Value = 2 },
                    new QuestionChoice(){ Text = "������������ ������� ����� (IT-��������, �������� �� ����������, ���������� ��������, ������������ �������� � �.�.)", Value = 3 },
                    new QuestionChoice(){ Text = "������������ �������� �����", Value = 4 },
                    new QuestionChoice(){ Text = "����������������� ����������, �������� (������ ������������ ������� ������� �����������)", Value = 5 }
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "s4",
                QuestionType = QuestionType.Text,
                Text = "��� ������ ���������� ���� ���������? "
            });
            context.Questions.Add(new Question()
            {
                Code = "s5",
                Text = "��� ���������� ���� �����������, ������ �� �������� ����� ������������?",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text ="������������", Value = 1 },
                    new QuestionChoice(){ Text ="��������� �������� ������� � ������", Value = 2 },
                    new QuestionChoice(){ Text ="����, ����, ���������", Value = 3 },
                    new QuestionChoice(){ Text ="����������", Value = 4 },
                    new QuestionChoice(){ Text ="����������", Value = 5 },
                    new QuestionChoice(){ Text ="��������� �������� ���������� �������", Value = 6 },
                    new QuestionChoice(){ Text ="���������", Value = 7 },
                    new QuestionChoice(){ Text ="�����", Value = 8 },
                    new QuestionChoice(){ Text ="������-������", Value = 9 },
                    new QuestionChoice(){ Text ="�������� ������", Value = 10 },
                    new QuestionChoice(){ Text ="������", Value = 99 }
                }
            });

            context.Questions.Add(new Question()
            {
                Code = "q1",
                Text = "����������, �������, ��������� � ����� ��� �������� ��� �� �������� �������� ���� �������?",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q1_1",
                QuestionType = QuestionType.Text,
                Text =
                    "�������, ��� ��� ����������� � �������� ���� �������? ����������, ������������ ���� �������� ����������� �����. � ��� ��� ��� �����������?"
            });
            context.Questions.Add(new Question()
            {
                Code = "q1_2",
                QuestionType = QuestionType.Text,
                Text = "� ��� ��� �� ����������� � �������� ���� �������?"
            });
            context.Questions.Add(new Question()
            {
                Code = "q2",
                Text = "����������� �� ��������, ������ ����������� ����, ��� ���� �������� �������� � ��������������� Market Player ��� ����, ����� ������ � ������� ��������� � �������� ��������� � ���������?",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text =  "����������� ��������", Value = 1},
                    new QuestionChoice(){ Text =  "������ ��������", Value = 2},
                    new QuestionChoice(){ Text =  "����� ��������, � ����� ���", Value = 3},
                    new QuestionChoice(){ Text =  "������ �� ��������", Value = 4},
                    new QuestionChoice(){ Text =  "����������� �� ��������", Value = 5},
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q2_1",
                QuestionType = QuestionType.Text,
                Text = "�� �������, ��� �� ��������� � ���������-�������������� � ��������� � ��������� ���� �������. �������, ����������, ������?"
            });
            context.Questions.Add(new Question()
            {
                Code = "q3",
                QuestionType = QuestionType.Choice,
                Text = "��� ������� ��������� � ���������� �� ������ ������� ������������",
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q4",
                Text = "���� ������� �������� ��� � ���� ��������",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q5",
                Text = "� ������� ����, � ��� ��������� � ���� ��������",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q6",
                Text = "��� ������� ��, ��� ��������� � ���� ��������",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q7",
                Text = "��� �� �������, ��������� �������� Market Player �������� ��� �� �������� �������, �������� ������� �� ������ ��� ���������? ",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text =  "��������� ��������", Value = 1},
                    new QuestionChoice(){ Text =  "������ ��������", Value = 2},
                    new QuestionChoice(){ Text =  "������� ��������, ������� ���", Value = 3},
                    new QuestionChoice(){ Text =  "������ �� �������������", Value = 4},
                    new QuestionChoice(){ Text =  "���������� �� ��������", Value = 5},
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q8",
                QuestionType = QuestionType.Text,
                Text = "�������, �� ������ ������, ��������� ������ �� ��������� ������������ �������� ���� ������� ����� � ����� ���������� ��� �������� �����? �� ������ ������������ ����� ���� �� 1 �� 7, ��������� ������� ����������� ���� ������."
            });
            context.Questions.Add(new Question()
            {
                Code = "q8_1",
                QuestionType = QuestionType.Choice,
                Text = "����������� / ������� �������",
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                    new QuestionChoice(){ Text = "����������� ��������", Value = 0}
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q8_2",
                Text = "������������� �������, ������������ ������� ����������",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                    new QuestionChoice(){ Text = "����������� ��������", Value = 0}
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q8_3",
                Text = "������� �������, ������� ����� ��������",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                    new QuestionChoice(){ Text = "����������� ��������", Value = 0}
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q8_4",
                Text = "���������������� ������������� ����������",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                    new QuestionChoice(){ Text = "����������� ��������", Value = 0}
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q8_5",
                Text = "������ � ������ � �������������",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                    new QuestionChoice(){ Text = "����������� ��������", Value = 0}
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q8_6",
                Text = "����� ������� �������, ��������� �����, ����� � ��� �����������",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                    new QuestionChoice(){ Text = "����������� ��������", Value = 0}
                }
            });

            context.Questions.Add(new Question()
            {
                Code = "q8_7",
                Text = "�����������, ���������� �������",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                    new QuestionChoice(){ Text = "����������� ��������", Value = 0}
                }
            });


            context.Questions.Add(new Question()
            {
                Code = "q8_8",
                Text = "���������� ������� �� ���������� ��������",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                    new QuestionChoice(){ Text = "����������� ��������", Value = 0}
                }
            });


            context.Questions.Add(new Question()
            {
                Code = "q8_9",
                Text = "�������� ������ ������� ��������� � ������� ����� �������� �����",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                    new QuestionChoice(){ Text = "����������� ��������", Value = 0}
                }
            });


            context.Questions.Add(new Question()
            {
                Code = "q8_10",
                Text = "�������� ��� �������� ������ �������",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                    new QuestionChoice(){ Text = "����������� ��������", Value = 0}
                }
            });


            context.Questions.Add(new Question()
            {
                Code = "q8_11",
                Text = "�����, ���������������� �������, �������� � �������",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                    new QuestionChoice(){ Text = "����������� ��������", Value = 0}
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q8_12",
                Text = "����������� ������� ���������� ������ ����� ���������� � IT",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                    new QuestionChoice(){ Text = "����������� ��������", Value = 0}
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q8_13",
                Text = "� ���� �������� �� ����� ����� �������� ����� ��������",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                    new QuestionChoice(){ Text = "����������� ��������", Value = 0}
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q8_14",
                Text = "� � ������� ����� ��������� ���������� �����������",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                    new QuestionChoice(){ Text = "����������� ��������", Value = 0}
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q8_15",
                Text = "������ ����������, ������������� ��������� � �������� ������",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                    new QuestionChoice(){ Text = "����������� ��������", Value = 0}
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q8_16",
                Text = "��� ������� �������� ��������� ������� ����� ��������",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "1", Value = 1 },
                    new QuestionChoice(){ Text = "2", Value = 2 },
                    new QuestionChoice(){ Text = "3", Value = 3 },
                    new QuestionChoice(){ Text = "4", Value = 4 },
                    new QuestionChoice(){ Text = "5", Value = 5 },
                    new QuestionChoice(){ Text = "6", Value = 6 },
                    new QuestionChoice(){ Text = "7", Value = 7 },
                    new QuestionChoice(){ Text = "����������� ��������", Value = 0}
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "q9",
                QuestionType = QuestionType.ConceptWords,
                Text = "����������, �������� ����� � ���� ��������, ������� �������� �������� ������� ��� ���, ���� ����� ����.<br/> ����������, �������� ��� ���������� �����. ���� ����� ���� ���, �������� �������."
            });
            context.Questions.Add(new Question()
            {
                Code = "q10",
                QuestionType = QuestionType.ConceptWords,
                Text = "����� �������� �����, ������� �� �������� �������� ������� ��� ���, ���� ����� ����.<br/> ����������, �������� ��� ���������� �����. ���� ����� ���� ���, �������� �������."
            });
            context.Questions.Add(new Question()
            {
                Code = "q11",
                QuestionType = QuestionType.ConceptWords,
                Text = "������ �������� ��, ��� �� �������� �������� ����� � ���������� ���� ������� �� ������, ���� ����?<br/> ����������, �������� ��� ���������� �����. ���� ����� ���� ���, �������� �������."
            });
            context.Questions.Add(new Question()
            {
                Code = "q12",
                QuestionType = QuestionType.ConceptWords,
                Text = "������ �������� �����, ������� �� �������� ����������� � �������� ��� ���������, ���� ����� ����.<br/> ����������, �������� ��� ���������� �����. ���� ����� ���� ���, �������� �������."
            });
            context.Questions.Add(new Question()
            {
                Code = "q13",
                QuestionType = QuestionType.ConceptWords,
                Text = "������� �������� �����, �������, �� ��� ������, �� �������� �������, ���� ����� ����.<br/> ����������, �������� ��� ���������� �����. ���� ����� ���� ���, �������� �������."
            });
            context.Questions.Add(new Question()
            {
                Code = "a1",
                QuestionType = QuestionType.Choice,
                Text = "������ ����� ������� ������ ������� � �������� �������, ���� ���� �������� ����� ��������� ��������� ������� Market Player, �������� ������� �� ������ ���������, ��� ��������?",
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text =  "�� ������ ����� ��������� �������", Value = 1},
                    new QuestionChoice(){ Text =  "�� ������ ����������� � �������� �������", Value = 2},
                    new QuestionChoice(){ Text =  "�� ������ ����������� � �������� �������", Value = 3}
                }

            });
            context.Questions.Add(new Question()
            {
                Code = "a2",
                Text = "������� ����� ����������� �������� � ����� ����������� � ������ ���� ��������, ���� ��� ����?",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text =  "����� 20 �������", Value = 1},
                    new QuestionChoice(){ Text =  "20-100 �������", Value = 2},
                    new QuestionChoice(){ Text =  "101-250 �������", Value = 3},
                    new QuestionChoice(){ Text =  "251-500 �������", Value = 4},
                    new QuestionChoice(){ Text =  "501-1000 �������", Value = 5},
                    new QuestionChoice(){ Text =  "����� 1000 �������", Value = 6},
                    new QuestionChoice(){ Text =  "����������� ��������", Value = 99},
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "a3",
                Text = "����� ���������� �������� ����� � ����� ��������?",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "����� 20", Value = 1 },
                    new QuestionChoice(){ Text = "21-50", Value = 2 },
                    new QuestionChoice(){ Text = "51-100", Value = 3 },
                    new QuestionChoice(){ Text = "101-500", Value = 4 },
                    new QuestionChoice(){ Text = "501-2000", Value = 5 },
                    new QuestionChoice(){ Text = "����� 2000", Value = 6 }
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "a4",
                Text = "�������� ����� ����� �������� ��������� � ����� ������ ��� � ���������� �������?",
                QuestionType = QuestionType.Choice,
                Choices = new List<QuestionChoice>()
                {
                    new QuestionChoice(){ Text = "� ����� ������", Value = 1 },
                    new QuestionChoice(){ Text = "���������� �������", Value = 2 }
                }
            });
            context.Questions.Add(new Question()
            {
                Code = "a5",
                Text = "������� ������� ���������� �������� ����� � ����� ������?",
                QuestionType = QuestionType.Text
            });
        }
    }
}
