namespace MarketPlayer.Survey.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Questions2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Questions", "Code", c => c.String(maxLength: 50));
            CreateIndex("dbo.Questions", "Code", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Questions", new[] { "Code" });
            AlterColumn("dbo.Questions", "Code", c => c.String());
        }
    }
}
