namespace MarketPlayer.Survey.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Questions3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.QuestionChoices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        Value = c.Int(nullable: false),
                        QuestionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .Index(t => t.QuestionId);
            
            AddColumn("dbo.Questions", "QuestionType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.QuestionChoices", "QuestionId", "dbo.Questions");
            DropIndex("dbo.QuestionChoices", new[] { "QuestionId" });
            DropColumn("dbo.Questions", "QuestionType");
            DropTable("dbo.QuestionChoices");
        }
    }
}
