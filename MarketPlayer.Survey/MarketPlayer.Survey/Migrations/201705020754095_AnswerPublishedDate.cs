namespace MarketPlayer.Survey.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AnswerPublishedDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Answers", "PublishedDateUTC", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Answers", "PublishedDateUTC");
        }
    }
}
